﻿using System.Web;
using System.Web.Optimization;

namespace AHC_MLK
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/vendor/assets/js/jquery.min.js",
                        "~/vendor/assets/js/popper.min.js",
                        "~/vendor/assets/js/bootstrap.min.js",
                        "~/vendor/assets/js/modernizr.min.js",
                        "~/vendor/assets/js/detect.min.js",
                        "~/vendor/assets/js/jquery.slimscroll.min.js",
                        "~/vendor/assets/js/sidebar-menu.min.js",
                        "~/vendor/assets/js/chartist.min.js",
                        "~/vendor/assets/js/chartist-plugin-tooltip.min.js",
                        "~/vendor/assets/js/to-do-list-init.min.js",
                        "~/vendor/assets/js/datepicker.min.js",
                        "~/vendor/assets/js/datepicker.en.min.js",
                        "~/vendor/assets/js/dashborad.min.js",
                        "~/vendor/assets/js/main.min.js"
                        ));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      
                      "~/vendor/assets/css/chartist.min.css",
                      "~/vendor/assets/css/datepicker.min.css",
                      "~/vendor/assets/css/summernote-bs4.css",
                      "~/vendor/assets/css/codemirror.css",
                      "~/vendor/assets/css/bootstrap.min.css",
                      "~/vendor/assets/css/icons.css",
                      "~/Content/jquery-ui.css",
                      "~/vendor/assets/css/style.css",
                      "~/Content/customer.css"
                      ));
        }
    }
}
