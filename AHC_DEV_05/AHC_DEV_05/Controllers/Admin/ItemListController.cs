﻿using AHCBL.Dao.Admin;
using AHCBL.Dto;
using AHCBL.Dto.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using AHCBL.Dao;
using AHCBL.Component.Common;
using System.Configuration;

namespace AHC_MLK.Controllers.Admin
{

    public class ItemListController : Controller
    {
        Permission checkuser = new Permission();
        // GET: ItemList
        public ActionResult Index(string search, int? page)
        {
            checkuser.chkrights("admin");
            TempData["search"] = search;
            ViewBag.product_id = DropdownDao.Instance.GetCatagoryList();
            //var list = ItemListDao.Instance.GetDataList();

            var data = ItemListDao.Instance.GetDataList();
            int rows = Util.NVLInt(ConfigurationManager.AppSettings["rows"]);

            ViewBag.Rows = rows;
            ViewBag.Count = data.Count.ToString();
            return View(data.Where(x => x.name == search || search == null || search == "").ToList().ToPagedList(page ?? 1, rows));

            //return View(list.Where(x => x.name == search || search == null || search == "").ToList().ToPagedList(page ?? 1, 3));
        }
       
        public ActionResult Create()
        {
            try
            {
                var ItemNo = GetDataDao.Instance.GenItemNumber();
                ViewBag.ItemNo = ItemNo;
                ViewBag.product_id = DropdownDao.Instance.GetCatagoryList();
                return View();
            }
            catch(Exception e)
            {
                ViewBag.Message = "Error : " + e.Message;
                return View();
            }
        }
        [HttpPost]
        public ActionResult Create(ItemListDto model)
        {
            try
            {
                var ItemNo = GetDataDao.Instance.GenItemNumber();
                
                model.code = Util.NVLString(GetDataDao.Instance.GenItemNumber());
                string result = ItemListDao.Instance.SaveDataList(model, "add");
                    if (result != "OK")
                    {
                        ViewBag.Message = result;
                        ModelState.Clear();
                    }
                    else
                    {

                        ViewBag.product_id = DropdownDao.Instance.GetCatagoryList();
                        ViewBag.ItemNo = ItemNo;
                        //ViewBag.Status = TempData["Dropdown"];
                        ViewBag.Message = "Successfully !!";
                        ModelState.Clear();
                    }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message;
                return View();
            }
        }
        public ActionResult Edit(int id)
        {
            var category = DropdownDao.Instance.GetCatagoryList();
            var data = ItemListDao.Instance.GetDataList().Find(smodel => smodel.id == id);
            data.category_list = new SelectList(category, "Value", "Text");
            return View(data);
        }
        [HttpPost]
        public ActionResult Edit(ItemListDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //model.price_win = model.price_win.ToString().Contains(",")==true ? model.price_win.Replace(",", ""): model.price_win;
                    //model.price_interest = model.price_interest.Replace(",", "");
                    string result = ItemListDao.Instance.SaveDataList(model, "edit");
                    if (result != "OK")
                    {
                        ViewBag.Message = result;
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                    return View();
                }
                else
                {
                    return View();
                }
                
            }
            catch
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult Delete(ItemListDto model, string[] dataarr)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    foreach (string id in dataarr)
                    {
                        model.id = Util.NVLInt(id);
                        ItemListDao.Instance.SaveDataList(model, "del");
                    }
                    return Json("Deleted successfully !");
                }
                else
                {
                    return Json("Data not found !");
                }
                
            }
            catch (Exception e)
            {
                return Json("Error");
            }
        }

      
    }
}