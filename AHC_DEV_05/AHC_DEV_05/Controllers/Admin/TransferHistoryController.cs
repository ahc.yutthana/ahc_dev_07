﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace AHC_DEV_05.Controllers.Admin
{
    public class TransferHistoryController : Controller
    {
        Permission checkuser = new Permission();
        // GET: WithdrawHistotory
        public ActionResult Index(int? page, string drp, string keyword)
        {
            checkuser.chkrights("admin");
            var data = PointListDao.Instance.GetTransfer();
            int rows = Util.NVLInt(Varible.Config.page_rows);
            var count = data.Count;

            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
            TempData["data1"] = data.ToList();
            if (keyword != null)
            {
                if (drp == "username")
                {
                    TempData["data"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                    TempData["data1"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList();
                    count = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().Count;
                }
                if (drp == "date")
                {
                    TempData["data"] = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                    TempData["data1"] = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList();
                    count = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList().Count;
                }
            }
            Session["Search"] = DropdownDao.Instance.GetDrpSearch();
            Session["Count"] = count;
            Session["Rows"] = rows;
            Session["data"] = TempData["data1"];

            ViewBag.Search = Session["Search"];
            ViewBag.Count = Session["Count"];
            ViewBag.Rows = Session["Rows"];

            return View(TempData["data"]);
        }
        public ActionResult ExportToExcel()
        {
            try
            {
                var data = Session["data"] as List<PointListDto>;
                if (data != null)
                {
                    ExcelPackage.LicenseContext = LicenseContext.Commercial;
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    ExcelPackage Ep = new ExcelPackage();
                    ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
                    Sheet.Cells["A1"].Value = "회원아이디";
                    Sheet.Cells["B1"].Value = "이름";
                    Sheet.Cells["C1"].Value = "내용";
                    Sheet.Cells["D1"].Value = "수수료금액";
                    Sheet.Cells["E1"].Value = "금액";
                    Sheet.Cells["F1"].Value = "날짜 ";
                    int row = 2;
                    foreach (var item in data)
                    {
                        Sheet.Cells[string.Format("A{0}", row)].Value = item.username.ToString();
                        Sheet.Cells[string.Format("B{0}", row)].Value = item.fullname.ToString();
                        Sheet.Cells[string.Format("C{0}", row)].Value = item.name.ToString();
                        Sheet.Cells[string.Format("D{0}", row)].Value = item.amount.ToString();
                        Sheet.Cells[string.Format("E{0}", row)].Value = item.price.ToString();
                        Sheet.Cells[string.Format("F{0}", row)].Value = item.create_date.ToString();
                        row++;
                    }
                    Sheet.Cells["A:AZ"].AutoFitColumns();
                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment: filename=" + "Report.xlsx");
                    Response.BinaryWrite(Ep.GetAsByteArray());
                    Response.End();
                }

                ViewBag.Search = Session["Search"];
                ViewBag.Count = Session["Count"];
                ViewBag.Rows = Session["Rows"];

                return View("Index");
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }
    }
}