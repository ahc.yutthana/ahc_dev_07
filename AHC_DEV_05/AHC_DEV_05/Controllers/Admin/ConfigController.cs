﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class ConfigController : Controller
    {
        Permission checkuser = new Permission();
        // GET: Config
        //public ActionResult Index()
        //{
        //    TempData["Skin"] = DropdownDao.Instance.GetSkin();
        //    ViewBag.member_id_list = new SelectList(Skin, "Value", "Text");
        //    ViewBag.new_skin = TempData["Skin"];
        //    ViewBag.mobile_new_skin = TempData["Skin"];
        //    ViewBag.search_skin = TempData["Skin"];
        //    ViewBag.mobile_search_skin = TempData["Skin"];
        //    ViewBag.connect_skin = TempData["Skin"];
        //    ViewBag.mobile_connect_skin = TempData["Skin"];
        //    ViewBag.faq_skin = TempData["Skin"];
        //    ViewBag.mobile_faq_skin = TempData["Skin"];
        //    ViewBag.member_id = DropdownDao.Instance.GetDataAdmin();
        //    //return View(ConfigDao.Instance.GetDataList().Find(smodel => smodel.id == 1));

        //    ViewBag.captcha = DropdownDao.Instance.GetDataCaptcha();

        //    ViewBag.link_target = DropdownDao.Instance.GetLink();
        //    ViewBag.member_skin = TempData["Skin"];
        //    ViewBag.mobile_member_skin = TempData["Skin"];
        //    TempData["level"] = DropdownDao.Instance.GetMemberLevel();
        //    ViewBag.register_level = TempData["level"];
        //    ViewBag.icon_level = TempData["level"];
        //    ViewBag.use_member_icon = DropdownDao.Instance.GetMemberIcon();
        //    ViewBag.cert_use = DropdownDao.Instance.GetIdCard();
        //    ViewBag.sms_use = DropdownDao.Instance.GetSMS();
        //    ViewBag.sms_type = DropdownDao.Instance.GetSMSType();
        //    return View(ConfigDao.Instance.GetDataList());
        //}

        public ActionResult Index()
        {
            checkuser.chkrights("admin");
            int id = 1;
            var Skin = DropdownDao.Instance.GetSkin();
            var cert_use = DropdownDao.Instance.GetIdCard();
            //ViewBag.member_id = TempData["Skin"];

            var model = ConfigDao.Instance.GetDataList().Find(smodel => smodel.id == id);
            var adm = DropdownDao.Instance.GetDataAdmin();
            model.member_id_list = new SelectList(adm, "Value", "Text");
            model.exchange = Util.NVLString(model.exchange).Replace(".00","");
            model.jewel_price = Util.NVLString(model.jewel_price).Replace(".00", "");
            model.day_max = Util.NVLString(model.day_max).Replace(".00", "");

            model.cert_use_list = new SelectList(cert_use, "Value", "Text");


            model.new_skin_list = new SelectList(Skin, "Value", "Text");
            model.mobile_new_skin_list = new SelectList(Skin, "Value", "Text");
            model.search_skin_list = new SelectList(Skin, "Value", "Text");
            model.mobile_search_skin_list = new SelectList(Skin, "Value", "Text");
            model.connect_skin_list = new SelectList(Skin, "Value", "Text");
            model.mobile_connect_skin_list = new SelectList(Skin, "Value", "Text");
            model.faq_skin_list = new SelectList(Skin, "Value", "Text");
            model.mobile_faq_skin_list = new SelectList(Skin, "Value", "Text");
            var captcha = DropdownDao.Instance.GetDataCaptcha();
            model.captcha_list = new SelectList(captcha, "Value", "Text");
            var link_target = DropdownDao.Instance.GetLink();
            model.link_target_list = new SelectList(link_target, "Value", "Text");
            model.member_skin_list = new SelectList(Skin, "Value", "Text");
            model.mobile_member_skin_list = new SelectList(Skin, "Value", "Text");

            var level = DropdownDao.Instance.GetMemberLevel();
            model.register_level_list = new SelectList(level, "Value", "Text");
            model.icon_level_list = new SelectList(level, "Value", "Text");


            var use_member_icon = DropdownDao.Instance.GetMemberIcon();
            model.use_member_icon_list = new SelectList(use_member_icon, "Value", "Text");

            var sms_use = DropdownDao.Instance.GetSMS();
            model.sms_use_list = new SelectList(sms_use, "Value", "Text");

            var sms_type = DropdownDao.Instance.GetSMSType();
            model.sms_type_list = new SelectList(sms_type, "Value", "Text");

            var cert_ipin = DropdownDao.Instance.GetIPIN();
            model.cert_ipin_list = new SelectList(cert_ipin, "Value", "Text");
            var cert_hp = DropdownDao.Instance.GetCertHP();
            model.cert_hp_list = new SelectList(cert_hp, "Value", "Text");

            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(ConfigDto model)
        {
            try
            {
                string result = ConfigDao.Instance.SaveDataList(model);
                if (result != "OK")
                {
                    ViewBag.Message = result;
                }
                else
                {
                    Session["web_name"] = model.name;
                    //ViewBag.Msg = "Successfully !";
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");

            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
    }
}