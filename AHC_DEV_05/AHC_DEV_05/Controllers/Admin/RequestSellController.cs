﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto;
using AHCBL.Dto.Admin;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace AHC_MLK.Controllers.Admin
{
    public class RequestSellController : Controller
    {
        Permission checkuser = new Permission();
        // GET: RequestSell
        public ActionResult Index(int? page, string drp, string keyword)
        {

            checkuser.chkrights("admin");
            var data = RequestSellDao.Instance.GetDataRequestSellList(); 
            int rows = Util.NVLInt(Varible.Config.page_rows);
            var count = data.Count;
            ViewBag.Status = DropdownDao.Instance.GetStatusTran();
            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
            TempData["data1"] = data.ToList();
            if (keyword != null)
            {
                if (drp == "username")
                {
                    TempData["data"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                    TempData["data1"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList();
                    count = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().Count;
                }
                if (drp == "date")
                {
                    TempData["data"] = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                    TempData["data1"] = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList();
                    count = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList().Count;
                }
            }
            Session["Search"] = DropdownDao.Instance.GetDrpSearch();
            Session["Count"] = count;
            Session["Rows"] = rows;
            Session["data"] = TempData["data1"];

            ViewBag.Search = Session["Search"];
            ViewBag.Count = Session["Count"];
            ViewBag.Rows = Session["Rows"];

            return View(TempData["data"]);


        }
        public ActionResult getdata(string id,string member)
        {
            try
            {
                var Matching = RequestSellDao.Instance.GetDataMatching(id,member);
                ViewBag.Matching = Matching.ToList();
                ViewBag.MatchingCnt = Matching.Count();
                return Json(new returnsave { err = "0", errmsg = "OK" });
              
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "1", errmsg = "Error" });
            }

        }
        public ActionResult matchdata(int id,int id_sale,int amt_sale)
        {
            try
            {
                //Session.Remove("member_id");
                string result = RequestSellDao.Instance.ChkData(id, id_sale, amt_sale);
                if (result == "0")
                {
                    return Json(new returnsave { err = "1", errmsg = "Error." });
                }
                else if (result == "1")
                {

                    return Json(new returnsave { err = "0", errmsg = "@amt_buy > amt_sale" });
                }
                else if (result == "2")
                {
                    return Json(new returnsave { err = "0", errmsg = "@amt_buy < amt_sale" });
                }
                else
                {
                    return Json(new returnsave { err = "0", errmsg = "@amt_buy = amt_sale" });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "0", errmsg = "Error" });
            }
        }

    }
}