﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class QAConfigController : Controller
    {
        Permission checkuser = new Permission();
        // GET: QAConfig
        public ActionResult Index()
        {
            checkuser.chkrights("admin");
            var Skin = DropdownDao.Instance.GetSkin();
            var noti = DropdownDao.Instance.GetNoTi();
            
            var model = QAConfigDao.Instance.GetDataList().Find(smodel => smodel.id == 1);

            model.skin_list = new SelectList(Skin, "Value", "Text");
            model.mobile_skin_list = new SelectList(Skin, "Value", "Text");

            model.use_sms_list = new SelectList(noti, "Value", "Text");
            model.use_editor_list = new SelectList(noti, "Value", "Text");

            return View(model);
        }
       

        [HttpPost]
        public ActionResult Index(QAConfigDto model)
        {
            try
            {
                string result = QAConfigDao.Instance.SaveDataList(model);
                if (result != "OK")
                {
                    ViewBag.Message = result;
                }
                else
                {
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

    }
}
