﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto;
using AHCBL.Dto.Admin;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;


namespace AHC_MLK.Admin.Controllers
{
    public class MemberListController : Controller
    {
        Permission checkuser = new Permission();
        // GET: MemberList
        public ActionResult Index(int? page, string drp, string keyword)
        {
            checkuser.chkrights("admin");            
            var data = MemberListDao.Instance.GetDataList();
            int rows = Util.NVLInt(Varible.Config.page_rows);
            var count = data.Count;
            ViewBag.Status = DropdownDao.Instance.GetStatusTran();
            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
            TempData["data1"] = data.ToList();
            if (keyword != null)
            {
                if (drp == "username")
                {
                    TempData["data"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                    TempData["data1"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList();
                    count = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().Count;
                }
                if (drp == "date")
                {
                    TempData["data"] = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                    TempData["data1"] = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList();
                    count = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList().Count;
                }
            }
            Session["Search"] = DropdownDao.Instance.GetDrpSearch();
            Session["Count"] = count;
            Session["Rows"] = rows;
            Session["data"] = TempData["data1"];

            ViewBag.Search = Session["Search"];
            ViewBag.Count = Session["Count"];
            ViewBag.Rows = Session["Rows"];

            return View(TempData["data"]);
        }
        public ActionResult ChkUserPhone(string pUid, string user_id)
        {
            try
            {
                //Session.Remove("member_id");
                bool result = LoginDao.Instance.ChkPhone(pUid, user_id);
                if (result == true)
                {
                    return Json(new returnsave { err = "0", errmsg = "이 전화 번호는 이미 사용 중입니다. 다른 전화 번호를 사용하세요." });
                }
                else
                {
                    //Session["member_id"] = member_id;
                    return Json(new returnsave { err = "1", errmsg = "" });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "0", errmsg = "Error" });
            }


            //return objreturnsave;
        }
        public ActionResult ChkUserID(string pUid, string user_id)
        {
            try
            {
                bool result = LoginDao.Instance.ChkUser(pUid, user_id);
                if (result == false)// false = ไม่มี username นี้
                {
                    return Json(new returnsave { err = "0", errmsg = "사용 가능한 아이디 입니다.\n회원가입을 진행해 주세요." });
                }
                else
                {
                    //Session["member_id"] = member_id;
                    return Json(new returnsave { err = "1", errmsg = "사용중인 아이디 입니다. 다른 아이디를 입력해 주세요." });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "0", errmsg = "Error" });
            }


            //return objreturnsave;
        }
        public ActionResult Create()
        {
            checkuser.chkrights("admin");
            ViewBag.Bank = DropdownDao.Instance.GetDataBank();
            ViewBag.member_no = DropdownDao.Instance.GetMemberNo();
            return View();
        }
        int member_id = 0;
        // POST: MemberList/Create
        [HttpPost]
        public ActionResult Create(MemberListDto model)
        {
            try
            {
                string result = MemberListDao.Instance.SaveDataList(model, "add", ref member_id);
                if (result != "OK")
                {
                    ViewBag.Message = result;
                    ModelState.Clear();
                }
                else
                {
                    //ViewBag.Status = TempData["Dropdown"];
                    ViewBag.Message = "Successfully !!";
                    //KRPBL.Component.Common.Form.SetAlertMessage(this,"ไม่พบ User ID ที่ระบุ กรุณาตรวจสอบ");
                    ModelState.Clear();
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message;
                return View();
            }
        }

        // GET: MemberList/Edit/5
        public ActionResult Edit(int id)
        {

            checkuser.chkrights("admin");
            var level = DropdownDao.Instance.GetMemberNo();
            ViewBag.member_icon_width = Varible.Config.member_icon_width;
            ViewBag.member_icon_height = Varible.Config.member_icon_height;
            ViewBag.member_img_width = Varible.Config.member_img_width;
            ViewBag.member_img_height = Varible.Config.member_img_height;
            var data = MemberListDao.Instance.GetDataList().Find(smodel => smodel.id == id);
            data.Techniques = new SelectList(level, "Value", "Text");
            var bank_id = DropdownDao.Instance.GetDataBank();
            data.bank_list = new SelectList(bank_id, "Value", "Text");
            return View(data);
        }

        // POST: MemberList/Edit/5
        [HttpPost]
        public ActionResult Edit(MemberListDto model)
        {
            try
            {

                string result = MemberListDao.Instance.SaveDataList(model, "edit", ref member_id);
                if (result != "OK")
                {
                    ViewBag.Message = result;
                }
                else
                {
                    return RedirectToAction("Index");
                }


                return View();

            }
            catch
            {
                return View();
            }
        }

        public ActionResult Add(MemberListDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //model.id = id;
                    MemberListDao.Instance.SaveDataList(model, "mem", ref member_id);
                    return Json("Add Successfully !");
                }
                else
                {
                    return Json("Data not found !");
                }
            }
            catch (Exception e)
            {
                return Json("Error");
            }
        }
        public ActionResult Delete(MemberListDto model, string[] customerIDs)
        {
            try
            {
                foreach (string id in customerIDs)
                {
                    model.id = Util.NVLInt(id);
                    bool result = MemberListDao.Instance.checkProduct(Util.NVLInt(id));
                    if (result == true)// false = ไม่มี username นี้
                    {
                        MemberListDao.Instance.SaveDataList(model, "del", ref member_id);
                       
                    }
                    else
                    {
                        //Session["member_id"] = member_id;
                        return Json(new returnsave { err = "1", errmsg = "Failed to delete !" });
                    }
                }
                return Json(new returnsave { err = "0", errmsg = "Deleted successfully!" });

            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "1", errmsg = "Error" });
            }
            //try
            //{
            //    string result = MemberListDao.Instance.SaveDataList(model, "del");
            //    if (result == "OK")
            //    {
            //        ViewBag.Message = "Student Deleted Successfully";
            //    }
            //    return RedirectToAction("Index");
            //}
            //catch (Exception e)
            //{
            //    ViewBag.Message = "Error : " + e.Message.ToString();
            //    return View();
            //}
        }
        [HttpPost]
        public ActionResult UploadFiles(HttpPostedFileBase file)
        {
            FileUploadDao.Instance.Upload(file);
            return View();
        }

        public ActionResult ExportToExcel()
        {            
            try
            {
                var data = Session["data"] as List<MemberListDto>;
                if (data != null)
                {
                    ExcelPackage.LicenseContext = LicenseContext.Commercial;
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    ExcelPackage Ep = new ExcelPackage();
                    ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
                    Sheet.Cells["A1"].Value = "아이디";
                    Sheet.Cells["B1"].Value = "성함";
                    Sheet.Cells["C1"].Value = "추천인";
                    Sheet.Cells["D1"].Value = "상품1현황";
                    Sheet.Cells["E1"].Value = "상품2현황";
                    Sheet.Cells["F1"].Value = "상품3현황";

                    Sheet.Cells["G1"].Value = "수수료";
                    Sheet.Cells["H1"].Value = "수수료금액(원)";
                    Sheet.Cells["I1"].Value = "휴대폰";
                    Sheet.Cells["J1"].Value = "가입일";

                    int row = 2;
                    foreach (var item in data)
                    {
                        Sheet.Cells[string.Format("A{0}", row)].Value = item.username.ToString();
                        Sheet.Cells[string.Format("B{0}", row)].Value = item.fullname.ToString();
                        Sheet.Cells[string.Format("C{0}", row)].Value = item.adviser_name.ToString();
                        Sheet.Cells[string.Format("D{0}", row)].Value = item.p1_amt.ToString();
                        Sheet.Cells[string.Format("E{0}", row)].Value = item.p2_amt.ToString();
                        Sheet.Cells[string.Format("F{0}", row)].Value = item.p3_amt.ToString();

                        Sheet.Cells[string.Format("G{0}", row)].Value = item.point.ToString();
                        Sheet.Cells[string.Format("H{0}", row)].Value = item.memo.ToString();
                        Sheet.Cells[string.Format("I{0}", row)].Value = item.mobile.ToString();
                        Sheet.Cells[string.Format("J{0}", row)].Value = item.create_date.ToString();
                        row++;
                    }
                    Sheet.Cells["A:AZ"].AutoFitColumns();
                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment: filename=" + "Report.xlsx");
                    Response.BinaryWrite(Ep.GetAsByteArray());
                    Response.End();


                }
                ViewBag.Search = Session["Search"];
                ViewBag.Count = Session["Count"];
                ViewBag.Rows = Session["Rows"];

                return View("Index");
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }
    }
}