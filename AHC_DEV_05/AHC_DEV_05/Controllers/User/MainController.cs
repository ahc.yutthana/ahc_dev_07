﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dao.User;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.User
{
    public class MainController : Controller
    {
        Permission checkuser = new Permission();
        // GET: main
        public ActionResult Index()
        {
            checkuser.chkrights("user");
            ViewBag.amount = GetDataDao.Instance.GetPointAmount(Varible.User.member_id);
            var pm = GetDataDao.Instance.GetProductMember();
            ViewBag.PM = pm;

            int rows = Util.NVLInt(ConfigurationManager.AppSettings["rows"]);
            var data = OrderDao.Instance.GetOrderHis();
            var order = OrderDao.Instance.GetProductOrder(0, "");
            ViewBag.his_tb = data;
            ViewBag.cnt = data.Count();

            var interests = InterestDao.Instance.GetDataInterest().Find(smodel => smodel.id == Util.NVLString(Varible.User.member_id));
            ViewBag.interests = interests == null ? "0" : interests.interest.ToString();

            var model = MemberListDao.Instance.GetDataList().Find(smodel => smodel.id == Varible.User.member_id);
            decimal point = (GetDataDao.Instance.GetPointAmount(Varible.User.member_id) - Util.NVLInt(Varible.Config.register_point));
            ViewBag.amountCW = point.ToString("0,0", CultureInfo.InvariantCulture) + " Point  (￦ " + (point * Util.NVLDecimal(Varible.Config.jewel_price)).ToString("0,0", CultureInfo.InvariantCulture) + ")";
            ViewBag.point = GetDataDao.Instance.GetPointAmount(Varible.User.member_id);


            //var dataNotice = NoticeDao.Instance.GetDataQA();
            //ViewBag.dataNotice = dataNotice;
            //ViewBag.cntNotice = dataNotice.Count();
            int? page = null;
            var dataNotice = ContentListDao.Instance.GetDataList();
            ViewBag.dataNotice = dataNotice.ToList().ToPagedList(page ?? 1, Util.NVLInt(ConfigurationManager.AppSettings["rows"]));
            ViewBag.cntNotice = dataNotice.Count();
            return View(order);

            
        }
    }
}