﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Component.Common
{
    public class EConfig
    {
        public int id { get; set; }
        public string name { get; set; }
        public string member_id { get; set; }
        public string exchange { get; set; }
        public string jewel_price { get; set; }
        public string day_max { get; set; }
        public string admin_email { get; set; }
        public string email_name { get; set; }
        //public int active { get; set;}
        public bool active { get; set; }
        public string open_link { get; set; }
        public string wallet_address { get; set; }
        public int point_percent1 { get; set; }
        public int point_percent2 { get; set; }
        public int point_percent3 { get; set; }
        public int point_percent4 { get; set; }
        public int login_point { get; set; }
        public int memo_send_point { get; set; }
        public int cut_name { get; set; }
        public int nick_modify { get; set; }
        public int open_modify { get; set; }
        public int new_del { get; set; }
        public int memo_del { get; set; }
        public int visit_del { get; set; }
        public int popular_del { get; set; }
        public int login_minutes { get; set; }
        public int new_rows { get; set; }
        public int page_rows { get; set; }
        public int mobile_page_rows { get; set; }
        public int write_pages { get; set; }
        public int mobile_pages { get; set; }
        public int new_skin { get; set; }
        public int mobile_new_skin { get; set; }
        public int search_skin { get; set; }
        public int mobile_search_skin { get; set; }
        public int connect_skin { get; set; }
        public int mobile_connect_skin { get; set; }
        public int faq_skin { get; set; }
        public string mobile_faq_skin { get; set; }
        public int editor { get; set; }
        public int captcha { get; set; }
        public string recaptcha_site_key { get; set; }
        public string recaptcha_secret_key { get; set; }
        public bool use_copy_log { get; set; }
        public int point_term { get; set; }
        public string possible_ip { get; set; }
        public string intercept_ip { get; set; }
        public string analytics { get; set; }
        public string add_meta { get; set; }
        public string syndi_token { get; set; }
        public string syndi_except { get; set; }
        public string delay_sec { get; set; }
        public int link_target { get; set; }
        public string read_point { get; set; }
        public string write_point { get; set; }
        public string comment_point { get; set; }
        public string download_point { get; set; }
        public int search_part { get; set; }
        public string image_extension { get; set; }
        public string flash_extension { get; set; }
        public string movie_extension { get; set; }
        public string filter { get; set; }
        public int member_skin { get; set; }
        public int mobile_member_skin { get; set; }
        public bool use_homepage { get; set; }
        public bool req_homepage { get; set; }
        public bool use_addr { get; set; }
        public bool req_addr { get; set; }
        public bool use_tel { get; set; }
        public bool req_tel { get; set; }
        public bool use_hp { get; set; }
        public bool req_hp { get; set; }
        public bool use_signature { get; set; }
        public bool req_signature { get; set; }
        public bool use_profile { get; set; }
        public bool req_profile { get; set; }
        public int register_level { get; set; }
        public int register_point { get; set; }
        public int leave_day { get; set; }
        public int use_member_icon { get; set; }
        public int icon_level { get; set; }
        public int member_icon_size { get; set; }
        public int member_icon_width { get; set; }
        public int member_icon_height { get; set; }
        public int member_img_size { get; set; }
        public int member_img_width { get; set; }
        public int member_img_height { get; set; }
        public bool use_recommend { get; set; }
        public int recommend_point { get; set; }
        public string prohibit_id { get; set; }
        public string prohibit_email { get; set; }
        public string stipulation { get; set; }
        public string privacy { get; set; }
        public int cert_use { get; set; }
        public int url_id { get; set; }
        public bool email_use { get; set; }
        public bool use_email_certify { get; set; }
        // public bool use_certify { get; set; }
        public bool formmail_is_member { get; set; }
        public bool email_wr_super_admin { get; set; }
        public bool email_wr_group_admin { get; set; }
        public bool email_wr_board_admin { get; set; }
        public bool email_wr_write { get; set; }
        public bool email_wr_comment_all { get; set; }
        public bool email_mb_super_admin { get; set; }
        public bool email_mb_member { get; set; }
        public bool email_po_super_admin { get; set; }
        public bool social_login_use { get; set; }
        public bool social_naver { get; set; }
        public bool social_kakao { get; set; }
        public bool social_facebook { get; set; }
        public bool social_google { get; set; }
        public bool social_twitter { get; set; }
        public bool social_payco { get; set; }
        public string naver_clientid { get; set; }
        public string naver_secret { get; set; }
        public string naver_url { get; set; }
        public string facebook_appid { get; set; }
        public string facebook_secret { get; set; }
        public string facebook_url { get; set; }
        public string twitter_key { get; set; }
        public string twitter_secret { get; set; }
        public string twitter_url { get; set; }
        public string google_clientid { get; set; }
        public string google_secret { get; set; }
        public string googl_shorturl_apikey { get; set; }
        public string google_url { get; set; }
        public string google_url_api { get; set; }
        public string kakao_rest_key { get; set; }
        public string kakao_client_secret { get; set; }
        public string kakao_js_apikey { get; set; }
        public string kakao_url { get; set; }
        public string payco_clientid { get; set; }
        public string payco_secret { get; set; }
        public string payco_url { get; set; }
        public string add_script { get; set; }
        public int sms_use { get; set; }
        public int sms_type { get; set; }
        public string icode_id { get; set; }
        public string icode_pw { get; set; }
        public string icode_server_ip { get; set; }
        public string icode_server_port { get; set; }
        public string icode_server_register { get; set; }
        public string icode_server_credit { get; set; }
        public string subj1 { get; set; }
        public string subj2 { get; set; }
        public string subj3 { get; set; }
        public string subj4 { get; set; }
        public string subj5 { get; set; }
        public string subj6 { get; set; }
        public string subj7 { get; set; }
        public string subj8 { get; set; }
        public string subj9 { get; set; }
        public string subj10 { get; set; }
        public string txt1 { get; set; }
        public string txt2 { get; set; }
        public string txt3 { get; set; }
        public string txt4 { get; set; }
        public string txt5 { get; set; }
        public string txt6 { get; set; }
        public string txt7 { get; set; }
        public string txt8 { get; set; }
        public string txt9 { get; set; }
        public string txt10 { get; set; }

        public string bank_info { get; set; }
        public string bank_acc { get; set; }
    }
}