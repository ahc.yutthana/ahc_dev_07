﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;

namespace AHCBL.Component.Common
{
    public static class FileUpload
    {
        
        public static void UploadFiles(HttpPostedFileBase file)
        {
            if (file != null)
            {
                string folder = ConfigurationManager.AppSettings["path"];
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                string extension = Path.GetExtension(file.FileName);
                string path = Path.Combine(folder, "img_" + DateTime.Now.ToString("yyMMddHHmmss")+extension);
                file.SaveAs(path);
                    
                

            }
        }

    }
}