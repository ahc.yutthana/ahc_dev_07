﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AHCBL.Dto.Admin
{
    public class FAQDto
    {
        public int id { get; set; }

        [MaxLength(70)]
        public string name { get; set; }

        [MaxLength(50)]
        public string img_head { get; set; }

        [MaxLength(50)]
        public string img_tail { get; set; }

        [AllowHtml]
        [MaxLength(255)]
        public string detail_hot { get; set; }

        [AllowHtml]
        [MaxLength(255)]
        public string detail_tail { get; set; }

        [AllowHtml]
        [MaxLength(255)]
        public string detail_hot_mobile { get; set; }

        [AllowHtml]
        [MaxLength(255)]
        public string detail_tail_mobile { get; set; }
        public int faq_order { get; set; }
        public int active { get; set; }

    }
}
