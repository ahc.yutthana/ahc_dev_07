﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.Admin
{
    public class RequestListDto
    {
        public int id { get; set; }
        public string token { get; set; }
        public int item_id { get; set; }
        public string username { get; set; }
        public string fullname { get; set; }
        public string buy_item { get; set; }
        public string amount { get; set; }
        public string total { get; set; }
        public string price { get; set; }
        public string create_date { get; set; }
        public string start_date { get; set; }
        public string stop_date { get; set; }
        public int active { get; set; }

    }
    public class RequestSumListDto
    {
        public string num { get; set; }
        public string username { get; set; }
        public string fullname { get; set; }
        public string name { get; set; }
        public int amount { get; set; }
        public string price_interest { get; set; }
        public string create_date { get; set; }
    }
    public class RequestSellListDto
    {
        public string num { get; set; }
        public string username { get; set; }
        public string fullname { get; set; }
        public string name { get; set; }
        public string price_interest { get; set; }
        public string amount { get; set; }
        public string create_date { get; set; }

    }
    public class ChkREQ
    {
        public int id { get; set; }
        public int amount { get; set; }
        public int amount_head { get; set; }
    }
    
}
