﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.Admin
{
    public class InterestDto
    {
        public string id { get; set; }
        public string username { get; set; }
        public string fullname { get; set; }
        public string interest { get; set; }
        public string point { get; set; }
        public string sale_date { get; set; }
    }
}
