﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.User
{
    public class ChartMemberDto
    {
        public int id { get; set; }
        public string username { get; set; }
        public int adviser_id { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string create_date { get; set; }
        public int master_id { get; set; }
        public int level_id { get; set; }
    }


}