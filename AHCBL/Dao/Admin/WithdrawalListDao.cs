﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class WithdrawalListDao : BaseDao<WithdrawalListDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<WithdrawalListDto> GetDataList()
        {
            try
            {
                List<WithdrawalListDto> list = new List<WithdrawalListDto>();
                dt = GetStoredProc("PD029_GET_WITHDRAWAL");
                foreach (DataRow dr in dt.Rows)
                {
                    int amt = Util.NVLInt(Util.NVLDecimal(dr["amount"]));
                    int persen = Util.NVLInt(ConfigurationManager.AppSettings["persen"]);
                    //int amount = (Util.NVLInt(dr["total"]) - (((amt / 100) * persen) + amt));
                    //int amount = (Util.NVLInt(dr["total"])/ (((amt / 100) * persen) + amt));
                    int total = Util.NVLInt(dr["total"]);
                    int charge = ((total /100)*persen);
                    int amount = total - charge;
                    list.Add(
                    new WithdrawalListDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        name = Util.NVLString(dr["name"]),
                        acc_name = Util.NVLString(dr["acc_name"]),
                        acc_no = Util.NVLString(dr["acc_no"]),
                        bank_name = Util.NVLString(dr["bank_name"]),
                        amount = Util.NVLString((amount).ToString("0,0", CultureInfo.InvariantCulture)),
                        charge = Util.NVLString(charge<1000? charge.ToString(): charge.ToString("0,0", CultureInfo.InvariantCulture)),
                        total = Util.NVLString(total.ToString("0,0", CultureInfo.InvariantCulture)),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        status = Util.NVLInt(dr["status"])
                    });

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string SaveDataList(WithdrawalListDto model, string action)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD030_SEVE_WITHDRAWAL", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@name", Util.NVLString(model.name));
                AddSQLParam(param, "@amount", Util.NVLString(model.amount));
                AddSQLParam(param, "@charge", Util.NVLString(model.charge));
                AddSQLParam(param, "@total", Util.NVLString(model.total));
                AddSQLParam(param, "@member_id", Util.NVLInt(Varible.User.member_id));
                AddSQLParam(param, "@p_status", Util.NVLString(model.status));
                AddSQLParam(param, "@action", action);
                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
    }
}