﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class QAConfigDao : BaseDao<QAConfigDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<QAConfigDto> GetDataList()
        {
            try
            {
                List<QAConfigDto> list = new List<QAConfigDto>();
                DataTable dt = GetStoredProc("PD017_GET_QA_CONFIG");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new QAConfigDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        title = Util.NVLString(dr["title"]),
                        category = Util.NVLString(dr["category"]),
                        directory_skin = Util.NVLInt(dr["directory_skin"]),
                        mobile_directory_skin = Util.NVLInt(dr["mobile_directory_skin"]),
                        use_email = Util.NVLBool(dr["use_email"]),
                        req_email = Util.NVLBool(dr["req_email"]),
                        //mobile_use_email = Util.NVLString(dr["mobile_use_email"]),
                        use_sms = Util.NVLBool(dr["use_sms"]),
                        use_hp = Util.NVLBool(dr["use_hp"]),
                        req_hp = Util.NVLBool(dr["req_hp"]),



                        send_number = Util.NVLString(dr["send_number"]),
                        mobile_admin = Util.NVLString(dr["mobile_admin"]),
                        email_admin = Util.NVLString(dr["email_admin"]),
                        use_editor = Util.NVLString(dr["use_editor"]),
                        subject_len = Util.NVLInt(dr["subject_len"]),
                        mobile_subject_len = Util.NVLInt(dr["mobile_subject_len"]),
                        page_rows = Util.NVLInt(dr["page_rows"]),
                        mobile_page_rows = Util.NVLInt(dr["mobile_page_rows"]),
                        image_width = Util.NVLInt(dr["image_width"]),
                        upload_size = Util.NVLInt(dr["upload_size"]),
                        include_head = Util.NVLString(dr["include_head"]),
                        include_tail = Util.NVLString(dr["include_tail"]),
                        detail_hots = Util.NVLString(dr["detail_hots"]),
                        detail_bottom = Util.NVLString(dr["detail_bottom"]),
                        mobile_detail_hots = Util.NVLString(dr["mobile_detail_hots"]),
                        mobile_detail_bottom = Util.NVLString(dr["mobile_detail_bottom"]),
                        insert_content = Util.NVLString(dr["insert_content"]),
                        subj1 = Util.NVLString(dr["subj1"]),
                        subj2 = Util.NVLString(dr["subj2"]),
                        subj3 = Util.NVLString(dr["subj3"]),
                        subj4 = Util.NVLString(dr["subj4"]),
                        subj5 = Util.NVLString(dr["subj5"]),
                        txt1 = Util.NVLString(dr["txt1"]),
                        txt2 = Util.NVLString(dr["txt2"]),
                        txt3 = Util.NVLString(dr["txt3"]),
                        txt4 = Util.NVLString(dr["txt4"]),
                        txt5 = Util.NVLString(dr["txt5"])

                    });
                }
                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }


        public string SaveDataList(QAConfigDto model)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD018_SAVE_QA_CONFIG", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@title", Util.NVLString(model.title));
                AddSQLParam(param, "@category", Util.NVLString(model.category));
                AddSQLParam(param, "@directory_skin", Util.NVLInt(model.directory_skin));
                AddSQLParam(param, "@mobile_directory_skin", Util.NVLInt(model.mobile_directory_skin));
                AddSQLParam(param, "@use_email", Util.NVLString(model.use_email));
                AddSQLParam(param, "@req_email", Util.NVLString(model.req_email));

                //AddSQLParam(param, "@mobile_use_email", Util.NVLString(model.mobile_use_email));
                AddSQLParam(param, "@use_sms", Util.NVLString(model.use_sms));

                AddSQLParam(param, "@use_hp", Util.NVLString(model.use_hp));
                AddSQLParam(param, "@req_hp", Util.NVLString(model.req_hp));

                AddSQLParam(param, "@send_number", Util.NVLString(model.send_number));
                AddSQLParam(param, "@mobile_admin", Util.NVLString(model.mobile_admin));
                AddSQLParam(param, "@email_admin", Util.NVLString(model.email_admin));
                AddSQLParam(param, "@use_editor", Util.NVLString(model.use_editor));
                AddSQLParam(param, "@subject_len", Util.NVLInt(model.subject_len));
                AddSQLParam(param, "@mobile_subject_len", Util.NVLInt(model.mobile_subject_len));
                AddSQLParam(param, "@page_rows", Util.NVLInt(model.page_rows));
                AddSQLParam(param, "@mobile_page_rows", Util.NVLInt(model.mobile_page_rows));
                AddSQLParam(param, "@image_width", Util.NVLInt(model.image_width));
                AddSQLParam(param, "@upload_size", Util.NVLInt(model.upload_size));
                AddSQLParam(param, "@include_head", Util.NVLString(model.include_head));
                AddSQLParam(param, "@include_tail", Util.NVLString(model.include_tail));
                AddSQLParam(param, "@detail_hots", Util.NVLString(model.detail_hots));
                AddSQLParam(param, "@detail_bottom", Util.NVLString(model.detail_bottom));
                AddSQLParam(param, "@mobile_detail_hots", Util.NVLString(model.mobile_detail_hots));
                AddSQLParam(param, "@mobile_detail_bottom", Util.NVLString(model.mobile_detail_bottom));
                AddSQLParam(param, "@insert_content", Util.NVLString(model.insert_content));
                AddSQLParam(param, "@subj1", Util.NVLString(model.subj1));
                AddSQLParam(param, "@subj2", Util.NVLString(model.subj2));
                AddSQLParam(param, "@subj3", Util.NVLString(model.subj3));
                AddSQLParam(param, "@subj4", Util.NVLString(model.subj4));
                AddSQLParam(param, "@subj5", Util.NVLString(model.subj5));
                AddSQLParam(param, "@txt1", Util.NVLString(model.txt1));
                AddSQLParam(param, "@txt2", Util.NVLString(model.txt2));
                AddSQLParam(param, "@txt3", Util.NVLString(model.txt3));
                AddSQLParam(param, "@txt4", Util.NVLString(model.txt4));
                AddSQLParam(param, "@txt5", Util.NVLString(model.txt5));
                AddSQLParam(param, "@member_id", Util.NVLInt(Varible.User.member_id));

                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
    }
}