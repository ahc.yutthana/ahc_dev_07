﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class InterestDao : BaseDao<InterestDao>
    {
        private DataTable dt;
        public List<InterestDto> GetDataInterest()
        {
            try
            {
                List<InterestDto> list = new List<InterestDto>();
                dt = GetStoredProc("PD086_GET_INTEREST");
                foreach (DataRow dr in dt.Rows)
                {
                    int point = (Util.NVLInt(dr["interest"]) / 100);
                    list.Add(
                    new InterestDto
                    {

                        id = Util.NVLString(dr["id"]),
                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        point = Util.NVLString(point < 1000 ? point.ToString() : point.ToString("0,0", CultureInfo.InvariantCulture)),
                        interest = Util.NVLString(Util.NVLInt(dr["interest"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        sale_date = dr["sale_date"].ToString() == "" ? "" : Util.NVLString(Cv.Date(Convert.ToDateTime(dr["sale_date"]).ToString("yyyyMMddHH:mm:ss"))),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
