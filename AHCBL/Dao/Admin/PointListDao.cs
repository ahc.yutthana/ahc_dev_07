﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class PointListDao : BaseDao<PointListDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<PointListDto> GetDataList(int pay_type)
        {
            try
            {
                List<PointListDto> list = new List<PointListDto>();
                dt = GetStoredProc("PD031_GET_POINT", new string[] { "@paytype" }, new string[] { Util.NVLString(pay_type) });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new PointListDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        name = Util.NVLString(dr["name"]),
                        amount = Util.NVLString(dr["amount"]),
                        exp_date = dr["exp_date"].ToString() =="" ? "" : "만료"+Util.NVLString(Convert.ToDateTime(dr["exp_date"]).ToString("yyyyMMdd")),
                        total = Util.NVLString(dr["total"]),
                        acc_no = Util.NVLString(dr["acc_no"]),
                        bank_name = Util.NVLString(dr["bank_name"]),
                        acc_name = Util.NVLString(dr["acc_name"]),
                        status = Util.NVLString(dr["status"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                    });

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<PointListDto> GetTransfer()
        {
            try
            {
                List<PointListDto> list = new List<PointListDto>();
                dt = GetStoredProc("PD084_GET_TRANFER_HISTORY");
                foreach (DataRow dr in dt.Rows)
                {
                    int amount = Util.NVLInt(dr["amount"]);
                    int price = Util.NVLInt(dr["price"]);
                    list.Add(
                    new PointListDto
                    {
                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        name = Util.NVLString(dr["name"]),
                        amount = Util.NVLString(amount < 1000 ? amount.ToString() : amount.ToString("0,0", CultureInfo.InvariantCulture)),
                        //Util.NVLString(dr["amount"]),
                        price = Util.NVLString(price < 1000 ? price.ToString() : price.ToString("0,0", CultureInfo.InvariantCulture)),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                    });

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<WithdrawalListDto> GetWithdraw()
        {
            try
            {
                List<WithdrawalListDto> list = new List<WithdrawalListDto>();
                dt = GetStoredProc("PD085_GET_WITHDRAW_HISTORY");
                foreach (DataRow dr in dt.Rows)
                {
                    int amt = Util.NVLInt(Util.NVLDecimal(dr["amount"]));
                    int persen = Util.NVLInt(ConfigurationManager.AppSettings["persen"]);
                    //int amount = (Util.NVLInt(dr["total"]) - (((amt / 100) * persen) + amt));
                    //int amount = (Util.NVLInt(dr["total"])/ (((amt / 100) * persen) + amt));
                    int total = Util.NVLInt(dr["total"]);
                    int charge = ((total / 100) * persen);
                    int amount = total - charge;
                    list.Add(
                    new WithdrawalListDto
                    {
                        /*
                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        name = Util.NVLString(dr["name"]),
                        amount = Util.NVLString(dr["amount"]),
                        price = Util.NVLString(dr["price"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        */

                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        name = Util.NVLString(dr["name"]),
                        acc_name = Util.NVLString(dr["acc_name"]),
                        acc_no = Util.NVLString(dr["acc_no"]),
                        bank_name = Util.NVLString(dr["bank_name"]),
                        amount = Util.NVLString((amount).ToString("0,0", CultureInfo.InvariantCulture)),
                        charge = Util.NVLString(charge < 1000 ? charge.ToString() : charge.ToString("0,0", CultureInfo.InvariantCulture)),
                        total = Util.NVLString(total.ToString("0,0", CultureInfo.InvariantCulture)),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        status = Util.NVLInt(dr["status"]),
                    });

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string TranPoint(int amt, int point, int p_id, string status)
        {
            string result = "OK";
            try
            {
                int cnt = Util.NVLInt(Util.NVLDecimal(Varible.Config.jewel_price));
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD060_TRAN_POINT", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_amount", Util.NVLInt(point));
                AddSQLParam(param, "@amt", Util.NVLInt(amt));
                AddSQLParam(param, "@price", Util.NVLString(cnt));
                AddSQLParam(param, "@p_id", Util.NVLInt(p_id));
                AddSQLParam(param, "@tran_id", Util.NVLInt(Varible.User.member_id));
                AddSQLParam(param, "@p_status", Util.NVLString(status));
                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
        public string SaveDataList(PointListDto model, string action)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD032_SAVE_POINT", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@name", Util.NVLString(model.name));
                AddSQLParam(param, "@amount", Util.NVLString(model.amount));
                AddSQLParam(param, "@total", Util.NVLString(model.total));
                if (model.exp_date != null)
                {
                    AddSQLParam(param, "@exp_date", Util.NVLString(model.exp_date));
                }
                else
                {
                    AddSQLParam(param, "@exp_date", "");
                }
                AddSQLParam(param, "@username", Util.NVLString(model.username));
                AddSQLParam(param, "@pay_type", Util.NVLString(model.pay_type));
                AddSQLParam(param, "@price", Util.NVLString(model.price));
                AddSQLParam(param, "@member_id", Util.NVLInt(model.member_id!=0 ? model.member_id : Varible.User.member_id));
                AddSQLParam(param, "@order_id", Util.NVLInt(0));
                AddSQLParam(param, "@status", action);
                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
        public string SaveDataList(RequestListDto model)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD072_APV_POINT", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@p_status", Util.NVLString(model.active));
                AddSQLParam(param, "@member_id", Util.NVLInt(Varible.User.member_id));
                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
    }
}