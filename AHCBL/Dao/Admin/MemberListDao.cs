﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
//using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class MemberListDao : BaseDao<MemberListDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<MemberListDto> GetDataList()
        {
            try
            {
                List<MemberListDto> list = new List<MemberListDto>();
                dt = GetStoredProc("PD003_GET_MEMBERS");
                foreach (DataRow dr in dt.Rows)
                {
                   
                    list.Add(
                        new MemberListDto
                        {
                            id = Util.NVLInt(dr["id"]),
                            username = Util.NVLString(dr["username"]),
                            password = Util.NVLString(DataCryptography.Decrypt(dr["password"].ToString())),
                            fullname = Util.NVLString(dr["fullname"]),
                            point = Util.NVLString(Util.NVLInt(dr["point"]) < 1000 ? dr["point"] : Util.NVLDecimal((dr["point"].ToString()).ToString()).ToString("0,0", CultureInfo.InvariantCulture)),
                            mobile = Util.NVLString(dr["mobile"]),
                            create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                            ip = Util.NVLString(dr["ip"]),
                            bank_id = Util.NVLInt(dr["bank_id"]),
                            acc_no = Util.NVLString(dr["acc_no"]),
                            acc_name = Util.NVLString(dr["acc_name"]),
                            memo = Util.NVLString(Util.NVLInt(dr["won"]) < 1000 ? dr["won"] : Util.NVLDecimal((dr["won"].ToString()).ToString()).ToString("0,0", CultureInfo.InvariantCulture)),
                            //last_date = Util.NVLString(dr["last_date"].ToString() == "" ? "0000-00-00 00:00:00" : Cv.Date(Convert.ToDateTime(dr["last_date"]).ToString("yyyyMMddHH:mm:ss"))),
                            adviser = Util.NVLInt(dr["adviser"]),
                            adviser_name = Util.NVLString(dr["adviser_name"]),

                            p1_amt = Util.NVLString(Util.NVLInt(dr["p1_amt"]) < 1000 ? dr["p1_amt"] : Util.NVLDecimal((dr["p1_amt"].ToString()).ToString()).ToString("0,0", CultureInfo.InvariantCulture)),
                            p2_amt = Util.NVLString(Util.NVLInt(dr["p2_amt"]) < 1000 ? dr["p2_amt"] : Util.NVLDecimal((dr["p2_amt"].ToString()).ToString()).ToString("0,0", CultureInfo.InvariantCulture)),
                            p3_amt = Util.NVLString(Util.NVLInt(dr["p3_amt"]) < 1000 ? dr["p3_amt"] : Util.NVLDecimal((dr["p3_amt"].ToString()).ToString()).ToString("0,0", CultureInfo.InvariantCulture)),



                            //leave_date = Util.NVLString(dr["leave_date"].ToString() == "" ? "" : Cv.Date(Convert.ToDateTime(dr["leave_date"]).ToString("yyyyMMdd"))),
                            //intercept_date = Util.NVLString(dr["intercept_date"].ToString() == "" ? "" : Cv.Date(Convert.ToDateTime(dr["intercept_date"]).ToString("yyyyMMdd"))),

                            /*
                            id = Util.NVLInt(dr["id"]),
                            username = Util.NVLString(dr["username"]),
                            password = Util.NVLString(DataCryptography.Decrypt(dr["password"].ToString())),
                            fullname = Util.NVLString(dr["fullname"]),
                            nickname = Util.NVLString(dr["nickname"]),
                            member_no = Util.NVLInt(dr["member_no"]),
                            level = Util.NVLInt(dr["member_level"]),
                            point = Util.NVLString(point),
                            email = Util.NVLString(dr["email"]),
                            homepage = Util.NVLString(dr["homepage"]),
                            telephone = Util.NVLString(dr["telephone"]),
                            mobile = Util.NVLString(dr["mobile"]),
                            certify_case = Util.NVLInt(dr["certify_case"]),
                            certify = Util.NVLInt(dr["certify"]),
                            adult = Util.NVLInt(dr["adult"]),
                            address = Util.NVLString(dr["address"]),
                            address1 = Util.NVLString(dr["address1"]),
                            address2 = Util.NVLString(dr["address2"]),
                            address3 = Util.NVLString(dr["address3"]),
                            icon = Util.NVLString(dr["icon"]),
                            img = Util.NVLString(dr["img"]),
                            mailling = Util.NVLString(dr["mailling"]),
                            sms = Util.NVLString(dr["sms"]),
                            open = Util.NVLString(dr["member_open"]),
                            signature = Util.NVLString(dr["signature"]),
                            profile = Util.NVLString(dr["profile"]),
                            memo = Util.NVLString(dr["memo"]),
                            create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                            //log_date = Util.NVLString(Vi),
                            ip = Util.NVLString(dr["ip"]),
                            bank_id = Util.NVLInt(dr["bank_id"]),
                            acc_no = Util.NVLString(dr["acc_no"]),
                            acc_name = Util.NVLString(dr["acc_name"]),
                            member_amount = Util.NVLInt(dr["member_amount"]),
                            last_date = Util.NVLString(dr["last_date"].ToString() =="" ? "0000-00-00 00:00:00" : Cv.Date(Convert.ToDateTime(dr["last_date"]).ToString("yyyyMMddHH:mm:ss"))),
                            adviser = Util.NVLInt(dr["adviser"]),
                            adviser_name = Util.NVLString(dr["adviser_name"]),
                            leave_date = Util.NVLString(dr["leave_date"].ToString() == "" ? "" : Cv.Date(Convert.ToDateTime(dr["leave_date"]).ToString("yyyyMMdd"))),
                            intercept_date = Util.NVLString(dr["intercept_date"].ToString() == "" ? "" : Cv.Date(Convert.ToDateTime(dr["intercept_date"]).ToString("yyyyMMdd"))),
                            txt1 = Util.NVLString(dr["txt1"]),
                            txt2 = Util.NVLString(dr["txt2"]),
                            txt3 = Util.NVLString(dr["txt3"]),
                            txt4 = Util.NVLString(dr["txt4"]),
                            txt5 = Util.NVLString(dr["txt5"]),
                            txt6 = Util.NVLString(dr["txt6"]),
                            txt7 = Util.NVLString(dr["txt7"]),
                            txt8 = Util.NVLString(dr["txt8"]),
                            txt9 = Util.NVLString(dr["txt9"]),
                            txt10 = Util.NVLString(dr["txt10"]),
                            status = Util.NVLInt(dr["status"])==1 ? "Active" : "Inactive"
                            */
                        });
                }
                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }

        public bool checkProduct(int id)
        {
            bool result = false;
            try
            {
                dt = GetStoredProc("PD096_CHK_PRODUCT", new string[] { "@p_id" }, new string[] { Util.NVLString(id) });
                //result = dt.Rows[].Count==0 ? true : false ;
                foreach (DataRow row in dt.Rows)
                {
                    int cnt = Util.NVLInt(row["cnt"]);
                    result = cnt > 0 ? false : true;
                }
                return result;
            }catch(Exception e)
            {
                result = false;
            }
            return result;
        }
        public string SaveDataList(MemberListDto model, string action, ref int member_id)
        {
            string result = "OK";
            try
            {


                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD001_SAVE_MEMBER", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@p_username", Util.NVLString(model.username));
                AddSQLParam(param, "@p_password", Util.NVLString(DataCryptography.Encrypt(model.password==null?"1234": model.password)));
                AddSQLParam(param, "@p_fullname", Util.NVLString(model.fullname));
                AddSQLParam(param, "@p_mobile", Util.NVLString(model.mobile));
                AddSQLParam(param, "@p_adviser", Util.NVLInt(model.adviser));
                AddSQLParam(param, "@p_ip", Util.NVLString(model.ip));

                AddSQLParam(param, "@p_bank_id", Util.NVLString(model.bank_id));
                AddSQLParam(param, "@p_acc_name", Util.NVLString(model.acc_name));
                AddSQLParam(param, "@p_acc_no", Util.NVLString(model.acc_no));
                AddSQLParam(param, "@action", action);
                AddSQLParam(param, "@user_id", action =="reg" ? 1: Util.NVLInt(Varible.User.member_id));
                AddSQLParam(param, "@out_id", "", MySqlDbType.Int32);
                cmd.Parameters["@out_id"].Direction = ParameterDirection.Output;

                conn.Open();
                //MySqlDataReader read = cmd.ExecuteReader();
                MySqlDataReader read = cmd.ExecuteReader();
                member_id = Util.NVLInt(cmd.Parameters["@out_id"].Value);
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }

                //cmd.ExecuteNonQuery();
                //member_id = Util.NVLInt(cmd.Parameters["@out_id"].Value);
                conn.Close();

                /*
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD001_SAVE_MEMBERS", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@username", Util.NVLString(model.username));
                //AddSQLParam(param, "@password", Util.NVLString(model.password));
                AddSQLParam(param, "@password", action == "del" ? "" : model.password ==null ? "" : Util.NVLString(DataCryptography.Encrypt(model.password)));
                AddSQLParam(param, "@fullname", Util.NVLString(model.fullname));
                AddSQLParam(param, "@nickname", Util.NVLString(model.nickname));
                //AddSQLParam(param, "@member_level", Util.NVLString(model.level));
                AddSQLParam(param, "@member_no", Util.NVLString(model.level));
               // AddSQLParam(param, "@point", Util.NVLInt(model.point));
                AddSQLParam(param, "@email", Util.NVLString(model.email));
                AddSQLParam(param, "@homepage", Util.NVLString(model.homepage));
                AddSQLParam(param, "@telephone", Util.NVLString(model.telephone));
                AddSQLParam(param, "@mobile", Util.NVLString(model.mobile));
                AddSQLParam(param, "@certify_case", Util.NVLString(model.certify_case));
                AddSQLParam(param, "@certify", Util.NVLString(model.certify));
                AddSQLParam(param, "@address", Util.NVLString(model.address));
                AddSQLParam(param, "@address1", Util.NVLString(model.address1));
                AddSQLParam(param, "@address2", Util.NVLString(model.address2));
                AddSQLParam(param, "@address3", Util.NVLString(model.address3));
                AddSQLParam(param, "@ip", Util.NVLString(GetDataDao.Instance.GetIp())); 
                AddSQLParam(param, "@p_icon", Util.NVLString(model.icon));
                AddSQLParam(param, "@p_img", Util.NVLString(model.img));
                AddSQLParam(param, "@mailling", Util.NVLBool(model.mailling));
                AddSQLParam(param, "@sms", Util.NVLBool(model.sms));
                AddSQLParam(param, "@member_open", Util.NVLBool(model.open));
                AddSQLParam(param, "@signature", Util.NVLString(model.signature));
                AddSQLParam(param, "@profile", Util.NVLString(model.profile));
                AddSQLParam(param, "@memo", Util.NVLString(model.memo));
                AddSQLParam(param, "@adviser", Util.NVLString(model.adviser));
                AddSQLParam(param, "@leave_date", Util.NVLString(model.leave_date));
                AddSQLParam(param, "@intercept_date", Util.NVLString(model.intercept_date));
                AddSQLParam(param, "@txt1", Util.NVLString(model.txt1));
                AddSQLParam(param, "@txt2", Util.NVLString(model.txt2));
                AddSQLParam(param, "@txt3", Util.NVLString(model.txt3));
                AddSQLParam(param, "@txt4", Util.NVLString(model.txt4));
                AddSQLParam(param, "@txt5", Util.NVLString(model.txt5));
                AddSQLParam(param, "@txt6", Util.NVLString(model.txt6));
                AddSQLParam(param, "@txt7", Util.NVLString(model.txt7));
                AddSQLParam(param, "@txt8", Util.NVLString(model.txt8));
                AddSQLParam(param, "@txt9", Util.NVLString(model.txt9));
                AddSQLParam(param, "@txt10", Util.NVLString(model.txt10));
                AddSQLParam(param, "@member_id", Util.NVLInt(1));
                AddSQLParam(param, "@bank_id", Util.NVLInt(model.bank_id));
                AddSQLParam(param, "@acc_no", Util.NVLString(model.acc_no));
                AddSQLParam(param, "@acc_name", Util.NVLString(model.acc_name));
                AddSQLParam(param, "@active", Util.NVLInt(model.active));
                AddSQLParam(param, "@status", action);
                AddSQLParam(param, "@out_id", "", MySqlDbType.Int32);
                cmd.Parameters["@out_id"].Direction = ParameterDirection.Output;

                conn.Open();
                //MySqlDataReader read = cmd.ExecuteReader();
                cmd.ExecuteNonQuery();
                member_id = Util.NVLInt(cmd.Parameters["@out_id"].Value);                
                conn.Close();
                */
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
    }
}

//cmd.Parameters.AddWithValue("@out_id", MySqlDbType.Int32);
//cmd.Parameters["@out_id"].Direction = ParameterDirection.Output;

//conn.Open();
//MySqlDataReader read = cmd.ExecuteReader();
//var outParamValue = cmd.Parameters["@out_id"].Value;