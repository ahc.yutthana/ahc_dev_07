﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class RequestListDao : BaseDao<RequestListDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<RequestListDto> GetDataList()
        {
            try
            {
                List<RequestListDto> list = new List<RequestListDto>();
                dt = GetStoredProc("PD027_GET_REQUEST");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new RequestListDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        token = Util.NVLString(DataCryptography.Encrypt(dr["product_id"].ToString())),
                        amount = Util.NVLString(dr["amount"]),
                        //total = Util.NVLString(Util.NVLDecimal(dr["total"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        total = Util.NVLString(dr["total"]),
                        price = Util.NVLDecimal(dr["price"]).ToString("0,0", CultureInfo.InvariantCulture),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        //stop_date = Util.NVLString(dr["stop_date"]),
                        active = Util.NVLInt(dr["status"]),
                    });

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RequestSumListDto> GetSummaryList()
        {
            try
            {
                List<RequestSumListDto> list = new List<RequestSumListDto>();
                dt = GetStoredProc("PD079_GET_REQUEST_LIST_SUMMARY");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new RequestSumListDto
                    {
                        num = Util.NVLString(dr["num"]),
                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        name = Util.NVLString(dr["name"]),
                        //buy_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["buy_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        amount = Util.NVLInt(dr["amount"]),
                        price_interest = Util.NVLDecimal(dr["price_interest"]).ToString("0,0", CultureInfo.InvariantCulture),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),

                    });

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<RequestSellListDto> GetSellList()
        {
            try
            {
                List<RequestSellListDto> list = new List<RequestSellListDto>();
                dt = GetStoredProc("PD083_GET_SELL_LIST_SUMMARY");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new RequestSellListDto
                    {
                        
                        num = Util.NVLString(dr["num"]),
                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        name = Util.NVLString(dr["name"]),
                        //buy_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["buy_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        price_interest = Util.NVLDecimal(dr["price_interest"]).ToString("0,0", CultureInfo.InvariantCulture),
                        amount = Util.NVLString(dr["amount"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                    });

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ChkREQ> ChkReq(int id)
        {
            try
            {
                List<ChkREQ> list = new List<ChkREQ>();
                dt = GetStoredProc("PD065_CHECK_REQUEST", new string[] { "@p_id" }, new string[] { Util.NVLString(id) });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new ChkREQ
                    {
                        id = Util.NVLInt(dr["id"]),
                        amount = Util.NVLInt(dr["amount"]),
                        amount_head = Util.NVLInt(dr["amount_head"]),
                    });

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string SaveReq(int id)
        {
            string result = "OK";
            try
            {
                dt = GetStoredProc("PD028_SAVE_REQUEST", new string[] { "@p_id" }, new string[] { Util.NVLString(id) });
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
        public string SaveDataList(RequestListDto model)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD028_SAVE_REQUEST", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@p_status", Util.NVLString(model.active));
                AddSQLParam(param, "@member_id", Util.NVLInt(Varible.User.member_id));
                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }

    }
}