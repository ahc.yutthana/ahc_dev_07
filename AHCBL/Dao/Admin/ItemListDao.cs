﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class ItemListDao : BaseDao<ItemListDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<ItemListDto> GetDataList()
        {
            try
            {
                List<ItemListDto> list = new List<ItemListDto>();
                dt = GetStoredProc("PD011_GET_PRODUCT_ORDER");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new ItemListDto
                    {
                        
                        id = Util.NVLInt(dr["id"]),
                        code = Util.NVLString(dr["code"]),
                        img = Util.NVLString(dr["img"]),
                        //status = Util.NVLString(dr["status"]),
                        amount = Util.NVLInt(dr["amount"]),
                        price_interest = Util.NVLString(Util.NVLInt(dr["price_interest"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        price_win = Util.NVLString(Util.NVLInt(dr["price_win"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        product_id = Util.NVLInt(dr["product_id"]),
                        buy_date = dr["buy_date"].ToString() == "" ? "" : Util.NVLString(Cv.Date(Convert.ToDateTime(dr["buy_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        tran_date = dr["tran_date"].ToString() == "" ? "" : Util.NVLString(Cv.Date(Convert.ToDateTime(dr["tran_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        create_date = dr["create_date"].ToString() == "" ? "" : Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        it_use = Util.NVLBool(dr["use"]),
                        buy_name = Util.NVLString(dr["buy_name"]),
                        

                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }


        public string SaveDataList(ItemListDto model, string action)
        {
            string result = "OK";            
            try
            {
                /*
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD012_SAVE_ITEM", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@category_id", Util.NVLInt(model.category_id));
                AddSQLParam(param, "@code", Util.NVLString(model.code));
                AddSQLParam(param, "@name", Util.NVLString(model.name));
                AddSQLParam(param, "@price_win", Util.NVLDecimal(model.price_win));
                AddSQLParam(param, "@buy_name", Util.NVLString(model.buy_name));
                AddSQLParam(param, "@start_date", Util.NVLString(model.start_date));
                AddSQLParam(param, "@it_use", Util.NVLBool(model.it_use));
                AddSQLParam(param, "@member_id", Util.NVLInt(Varible.User.member_id));
                AddSQLParam(param, "@status", action);

                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
                */

                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD012_SAVE_PRODUCT_ORDER", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@product_id", Util.NVLInt(model.product_id));
                AddSQLParam(param, "@p_amount", Util.NVLInt(model.amount));
                AddSQLParam(param, "@p_code", Util.NVLString(model.code));
                AddSQLParam(param, "@p_buy_name", Util.NVLString(model.buy_name));
                AddSQLParam(param, "@p_buy_date", Util.NVLString(model.buy_date));
                AddSQLParam(param, "@member_id", Util.NVLInt(10));
                AddSQLParam(param, "@status", action);

                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
    }
}