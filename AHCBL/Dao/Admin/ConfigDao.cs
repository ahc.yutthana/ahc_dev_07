﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class ConfigDao : BaseDao<ConfigDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<ConfigDto> GetDataList()
        {
            try
            {
                List<ConfigDto> list = new List<ConfigDto>();
                dt = GetStoredProc("PD035_GET_CONFIG");
                foreach (DataRow dr in dt.Rows)
                {
                    //var use_certify = Util.NVLBool(dr["use_email_certify"]);
                    //var aaa = DataCryptography.Decrypt(dr["icode_pw"].ToString());

                    list.Add(
                    new ConfigDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        name = Util.NVLString(dr["name"]),
                        member_id = Util.NVLString(dr["member_id"]),
                        exchange = Util.NVLString(dr["exchange"]),
                        jewel_price = Util.NVLString(dr["jewel_price"]),
                        day_max = Util.NVLString(dr["day_max"]),
                        admin_email = Util.NVLString(dr["admin_email"]),
                        email_name = Util.NVLString(dr["email_name"]),
                        //active = Util.NVLInt(dr["active"]),
                        active = Util.NVLBool(dr["active"]),
                        open_link = Util.NVLString(dr["open_link"]),
                        wallet_address = Util.NVLString(dr["wallet_address"]),
                        point_percent1 = Util.NVLInt(dr["point_percent1"]),
                        point_percent2 = Util.NVLInt(dr["point_percent2"]),
                        point_percent3 = Util.NVLInt(dr["point_percent3"]),
                        point_percent4 = Util.NVLInt(dr["point_percent4"]),
                        login_point = Util.NVLInt(dr["login_point"]),
                        memo_send_point = Util.NVLInt(dr["memo_send_point"]),
                        cut_name = Util.NVLInt(dr["cut_name"]),
                        nick_modify = Util.NVLInt(dr["nick_modify"]),
                        open_modify = Util.NVLInt(dr["open_modify"]),
                        new_del = Util.NVLInt(dr["new_del"]),
                        memo_del = Util.NVLInt(dr["memo_del"]),
                        visit_del = Util.NVLInt(dr["visit_del"]),
                        popular_del = Util.NVLInt(dr["popular_del"]),
                        login_minutes = Util.NVLInt(dr["login_minutes"]),
                        new_rows = Util.NVLInt(dr["new_rows"]),
                        page_rows = Util.NVLInt(dr["page_rows"]),
                        mobile_page_rows = Util.NVLInt(dr["mobile_page_rows"]),
                        write_pages = Util.NVLInt(dr["write_pages"]),
                        mobile_pages = Util.NVLInt(dr["mobile_pages"]),
                        new_skin = Util.NVLInt(dr["new_skin"]),
                        mobile_new_skin = Util.NVLInt(dr["mobile_new_skin"]),
                        search_skin = Util.NVLInt(dr["search_skin"]),
                        mobile_search_skin = Util.NVLInt(dr["mobile_search_skin"]),
                        connect_skin = Util.NVLInt(dr["connect_skin"]),
                        mobile_connect_skin = Util.NVLInt(dr["mobile_connect_skin"]),
                        faq_skin = Util.NVLInt(dr["faq_skin"]),
                        mobile_faq_skin = Util.NVLString(dr["mobile_faq_skin"]),
                        editor = Util.NVLInt(dr["editor"]),
                        captcha = Util.NVLInt(dr["captcha"]),
                        recaptcha_site_key = Util.NVLString(dr["recaptcha_site_key"]),
                        recaptcha_secret_key = Util.NVLString(dr["recaptcha_secret_key"]),
                        //use_copy_log = Util.NVLInt(dr["use_copy_log"]),
                        use_copy_log = Util.NVLBool(dr["use_copy_log"]),
                        point_term = Util.NVLInt(dr["point_term"]),
                        possible_ip = Util.NVLString(dr["possible_ip"]),
                        intercept_ip = Util.NVLString(dr["intercept_ip"]),
                        analytics = Util.NVLString(dr["analytics"]),
                        add_meta = Util.NVLString(dr["add_meta"]),
                        syndi_token = Util.NVLString(dr["syndi_token"]),
                        syndi_except = Util.NVLString(dr["syndi_except"]),
                        delay_sec = Util.NVLString(dr["delay_sec"]),
                        link_target = Util.NVLInt(dr["link_target"]),
                        read_point = Util.NVLString(dr["read_point"]),
                        write_point = Util.NVLString(dr["write_point"]),
                        comment_point = Util.NVLString(dr["comment_point"]),
                        download_point = Util.NVLString(dr["download_point"]),
                        search_part = Util.NVLInt(dr["search_part"]),
                        image_extension = Util.NVLString(dr["image_extension"]),
                        flash_extension = Util.NVLString(dr["flash_extension"]),
                        movie_extension = Util.NVLString(dr["movie_extension"]),
                        filter = Util.NVLString(dr["filter"]),
                        member_skin = Util.NVLInt(dr["member_skin"]),
                        mobile_member_skin = Util.NVLInt(dr["mobile_member_skin"]),
                        use_homepage = Util.NVLBool(dr["use_homepage"]),
                        req_homepage = Util.NVLBool(dr["req_homepage"]),
                        use_addr = Util.NVLBool(dr["use_addr"]),
                        req_addr = Util.NVLBool(dr["req_addr"]),
                        use_tel = Util.NVLBool(dr["use_tel"]),
                        req_tel = Util.NVLBool(dr["req_tel"]),
                        use_hp = Util.NVLBool(dr["use_hp"]),
                        req_hp = Util.NVLBool(dr["req_hp"]),
                        use_signature = Util.NVLBool(dr["use_signature"]),
                        req_signature = Util.NVLBool(dr["req_signature"]),
                        use_profile = Util.NVLBool(dr["use_profile"]),
                        req_profile = Util.NVLBool(dr["req_profile"]),
                        register_level = Util.NVLInt(dr["register_level"]),
                        register_point = Util.NVLInt(dr["register_point"]),
                        leave_day = Util.NVLInt(dr["leave_day"]),
                        use_member_icon = Util.NVLInt(dr["use_member_icon"]),
                        icon_level = Util.NVLInt(dr["icon_level"]),
                        member_icon_size = Util.NVLInt(dr["member_icon_size"]),
                        member_icon_width = Util.NVLInt(dr["member_icon_width"]),
                        member_icon_height = Util.NVLInt(dr["member_icon_height"]),
                        member_img_size = Util.NVLInt(dr["member_img_size"]),
                        member_img_width = Util.NVLInt(dr["member_img_width"]),
                        member_img_height = Util.NVLInt(dr["member_img_height"]),
                        use_recommend = Util.NVLBool(dr["use_recommend"]),
                        recommend_point = Util.NVLInt(dr["recommend_point"]),
                        prohibit_id = Util.NVLString(dr["prohibit_id"]),
                        prohibit_email = Util.NVLString(dr["prohibit_email"]),
                        stipulation = Util.NVLString(dr["stipulation"]),
                        privacy = Util.NVLString(dr["privacy"]),

                        cert_use = Util.NVLInt(dr["cert_use"]),
                        cert_ipin= Util.NVLInt(dr["cert_ipin"]),
                        cert_hp= Util.NVLInt(dr["cert_hp"]),
                        cert_kcb_cd= Util.NVLString(dr["cert_kcb_cd"]),
                        cert_kcp_cd= Util.NVLString(dr["cert_kcp_cd"]),
                        lg_mid= Util.NVLString(dr["lg_mid"]),
                        lg_mert_key= Util.NVLString(dr["lg_mert_key"]),
                        cert_limit= Util.NVLString(dr["cert_limit"]),
                        cert_req= Util.NVLBool(dr["cert_req"]),

                        url_id = Util.NVLInt(dr["url_id"]),
                        email_use = Util.NVLBool(dr["email_use"]),
                        use_email_certify = Util.NVLBool(dr["use_email_certify"]),
                        //use_certify = Util.NVLBool(dr["use_email_certify"]),
                        formmail_is_member = Util.NVLBool(dr["formmail_is_member"]),
                        email_wr_super_admin = Util.NVLBool(dr["email_wr_super_admin"]),
                        email_wr_group_admin = Util.NVLBool(dr["email_wr_group_admin"]),
                        email_wr_board_admin = Util.NVLBool(dr["email_wr_board_admin"]),
                        email_wr_write = Util.NVLBool(dr["email_wr_write"]),
                        email_wr_comment_all = Util.NVLBool(dr["email_wr_comment_all"]),
                        email_mb_super_admin = Util.NVLBool(dr["email_mb_super_admin"]),
                        email_mb_member = Util.NVLBool(dr["email_mb_member"]),
                        email_po_super_admin = Util.NVLBool(dr["email_po_super_admin"]),
                        social_login_use = Util.NVLBool(dr["social_login_use"]),
                        social_naver = Util.NVLBool(dr["social_naver"]),
                        social_kakao = Util.NVLBool(dr["social_kakao"]),
                        social_facebook = Util.NVLBool(dr["social_facebook"]),
                        social_google = Util.NVLBool(dr["social_google"]),
                        social_twitter = Util.NVLBool(dr["social_twitter"]),
                        social_payco = Util.NVLBool(dr["social_payco"]),
                        naver_clientid = Util.NVLString(dr["naver_clientid"]),
                        naver_secret = Util.NVLString(dr["naver_secret"]),
                        naver_url = Util.NVLString(dr["naver_url"]),
                        facebook_appid = Util.NVLString(dr["facebook_appid"]),
                        facebook_secret = Util.NVLString(dr["facebook_secret"]),
                        facebook_url = Util.NVLString(dr["facebook_url"]),
                        twitter_key = Util.NVLString(dr["twitter_key"]),
                        twitter_secret = Util.NVLString(dr["twitter_secret"]),
                        twitter_url = Util.NVLString(dr["twitter_url"]),
                        google_clientid = Util.NVLString(dr["google_clientid"]),
                        google_secret = Util.NVLString(dr["google_secret"]),
                        googl_shorturl_apikey = Util.NVLString(dr["googl_shorturl_apikey"]),
                        google_url = Util.NVLString(dr["google_url"]),
                        google_url_api = Util.NVLString(dr["google_url_api"]),
                        kakao_rest_key = Util.NVLString(dr["kakao_rest_key"]),
                        kakao_client_secret = Util.NVLString(dr["kakao_client_secret"]),
                        kakao_js_apikey = Util.NVLString(dr["kakao_js_apikey"]),
                        kakao_url = Util.NVLString(dr["kakao_url"]),
                        payco_clientid = Util.NVLString(dr["payco_clientid"]),
                        payco_secret = Util.NVLString(dr["payco_secret"]),
                        payco_url = Util.NVLString(dr["payco_url"]),
                        add_script = Util.NVLString(dr["add_script"]),
                        sms_use = Util.NVLInt(dr["sms_use"]),
                        sms_type = Util.NVLInt(dr["sms_type"]),
                        icode_id = Util.NVLString(dr["icode_id"]),
                        icode_pw = Util.NVLString(dr["icode_pw"]),
                        icode_server_ip = Util.NVLString(dr["icode_server_ip"]),
                        icode_server_port = Util.NVLString(dr["icode_server_port"]),
                        icode_server_register = Util.NVLString(dr["icode_server_register"]),
                        icode_server_credit = Util.NVLString(dr["icode_server_credit"]),
                        subj1 = Util.NVLString(dr["subj1"]),
                        subj2 = Util.NVLString(dr["subj2"]),
                        subj3 = Util.NVLString(dr["subj3"]),
                        subj4 = Util.NVLString(dr["subj4"]),
                        subj5 = Util.NVLString(dr["subj5"]),
                        subj6 = Util.NVLString(dr["subj6"]),
                        subj7 = Util.NVLString(dr["subj7"]),
                        subj8 = Util.NVLString(dr["subj8"]),
                        subj9 = Util.NVLString(dr["subj9"]),
                        subj10 = Util.NVLString(dr["subj10"]),
                        txt1 = Util.NVLString(dr["txt1"]),
                        txt2 = Util.NVLString(dr["txt2"]),
                        txt3 = Util.NVLString(dr["txt3"]),
                        txt4 = Util.NVLString(dr["txt4"]),
                        txt5 = Util.NVLString(dr["txt5"]),
                        txt6 = Util.NVLString(dr["txt6"]),
                        txt7 = Util.NVLString(dr["txt7"]),
                        txt8 = Util.NVLString(dr["txt8"]),
                        txt9 = Util.NVLString(dr["txt9"]),
                        txt10 = Util.NVLString(dr["txt10"]),

                    });

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string SaveDataList(ConfigDto model)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD038_SAVE_CONFIG", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                //AddSQLParam(param, "@memberid", Util.NVLString(model.member_id));
                AddSQLParam(param, "@name", Util.NVLString(model.name));
                AddSQLParam(param, "@exchange", Util.NVLString(model.exchange));
                AddSQLParam(param, "@jewel_price", Util.NVLString(model.jewel_price));
                AddSQLParam(param, "@wallet_address", Util.NVLString(model.wallet_address));
                AddSQLParam(param, "@register_point", Util.NVLInt(model.register_point));
                AddSQLParam(param, "@recommend_point", Util.NVLInt(model.recommend_point));


                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
    }
}